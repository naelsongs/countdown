
<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
          integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css"
          integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
    <title>Countdown</title>
    <link rel="stylesheet" type="text/css" href="style.css"/>
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script language="JavaScript" type="text/javascript">

        //Jan,Feb,Mar,Apr,Jun,Jul,Aug,Sep,Oct,Nov,Dec
        var day = 28;
        var month = "Oct";
        var year = 2018;
        var hour = "12:00:00";
        var nTime = "" + month + " " + day + ", " + year + " " + hour + "";
        var countDownDate = new Date(nTime).getTime();

        var dateCurrent = new Date();
        dateCurrent.setTime(countDownDate);

        // Update the count down every 1 second
        var x = setInterval(function () {

            // Get todays date and time
            var now = new Date().getTime();

            // Find the distance between now and the count down date
            var distance = countDownDate - now;

            // Time calculations for days, hours, minutes and seconds
            var days = Math.floor(distance / (1000 * 60 * 60 * 24));
            var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
            var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
            var seconds = Math.floor((distance % (1000 * 60)) / 1000);

            document.getElementById("demo").innerHTML =
                "<div class='countdown'><div class='title-countdown'></div><ul>" +
                "<li>" + days + "<div class='day'>Day</div></li>" +
                "<li>" + hours + "<div class='day'>Hour</div></li>" +
                "<li>" + minutes + "<div class='day'>Min</div></li>" +
                "<li>" + seconds + "<div class='day' style='top: -" + seconds + "px;color: rgba(255,255,255,0);'>sec</div></li>" +
                "</ul></div>";

            if (distance < 0) {clearInterval(x);document.getElementById("demo").innerHTML = "EXPIRED";}
            $(function () {

                $.ajax({
                    url: "file.php", type: "POST", data: {countdownKey: dateCurrent, countDown: days + hours + minutes + seconds}, dataType: 'json',
                    success: function (json) {
                        console.log("Successful countdownKey");
                        if (json.succes = true){
                            $('.title-countdown').html(json.text);
                            console.log("Result: " + json.text + "Count: " + json.countDown);
                        }

                    },
                    error: function (json) {console.log("Failed countdownKey");}
                });

            });
        }, 1000);
    </script>
</head>
<body>

<!-- Display the countdown timer in an element -->
<div id="demo">

</div>


<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
        integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"
        integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy"
        crossorigin="anonymous"></script>
<script type="text/javascript" src="global.js"></script>

</body>
</html>